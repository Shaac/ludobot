Werewolves IRC Bot
==================

Installation
------------

To compile the bot, just type `make`.
You want to modify the main.hs file for its configuration.

How does it work?
-----------------

It does not work yet, but one can launch a new game with "!start", join the
game with "!play", and then after one minute the game will launch, people will
get their cards, and werewolves will be invited to kill someone.

Why this bot?
-------------

Why make an other werewolf bot? Well, this one is in Haskell. And if it does
not work yet, it is beaucause it took time to make it with a structure capable
of making new cards easily, and not having to make some lines of code in the
hole code to add a functionnality. It everything works fine, it will just need
a block of code, even not that long.
