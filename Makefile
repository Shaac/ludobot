TARGET = main

all: $(TARGET)

clean:
	rm -f *.o
	rm -f *.hi
	rm -f $(TARGET)
	rm -f test

$(TARGET): $(TARGET).hs irc.hs werewolves.hs
	ghc --make -Wall -Werror $^
	rm *.hi
	rm *.o
