module Werewolves (Game (eval, actions), newGame, update, Action (..)) where

-- Import {{{
import Data.Map         (Map, empty, insert, size, elemAt, elems, mapWithKey)
import Data.Map         (delete, (!), filter, keys)
import Data.List        (nub)
import Prelude          hiding (filter)
import System.Time      (ClockTime (TOD), getClockTime)
import System.Random    (StdGen, getStdGen, randomRs)
-- }}}

-- Constants {{{

-- Function returning the string to be displayed
lang :: String -> String -> String
lang "villager" _ = "Villager"
lang "werewolf" _ = "Werewolf"
lang "start"    _ = "A new game is starting, press !play to join."
lang "endstart" _ = "The game will now begin."
lang "role"     r = "Your role is: " ++ r ++ "."
lang "join"     _ = "Please join the night channel: "
lang "wakeup"   _ = "It's time to wake up. Type !vote name to kill."
lang "sleep"    _ = "You can go to sleep now."
lang "dead"     p = p ++ " is dead this night."
lang "day"      _ = "Time to choose someone to kill. Type !vote name."
lang "end"      _ = "The game is over."
lang _          x = x

-- Function returning how long last a specific phase
time :: String -> Integer
time "wait"     = 60
time "werewolf" = 60
time "day"      = 60
time _          = 0

-- }}}

-- Define structures {{{

-- Represents the day chan, or the night chan, or an other string
data Action = Str String | Day | Night

-- Represents all data of the game
data Game = Game { players :: Map String Card
                 , votes   :: Map String String
                 , eval    :: [String] -> String -> Action -> Game -> IO Game
                 , next    :: Game -> IO Game
                 , timeout :: Integer
                 , actions :: [[Action]]
                 , gen     :: StdGen
                 }

-- Structure representing a card :
-- - name is the string representing the card's name
-- - handle is the function called by the other roles to determine what happens
-- - begin is the function called when the role is awaked
data Card = Card { name   :: String
                 , handle :: String -> (Game -> Game) -> Game -> Game
                 , begin  :: Game -> IO Game
                 }
instance Eq Card where
    c == c' = name c == name c'

-- }}}

-- Core functions {{{

-- Return a new game, with a random seed generated
newGame :: IO Game
newGame = getStdGen >>= return . (Game empty empty wait start 4294967296 [])

-- Update a game if the time of the current phase is out
update :: Game -> IO Game
update g = do
    TOD clock _ <- getClockTime
    if clock >= timeout g
       then next g g
       else return g

-- Start the game (everyone is ready)
start :: Game -> IO Game
start g = begin werewolf $ g {players = players', actions = roles ++ actions'}
    where
        roles    = concatMap id $ elems $ mapWithKey send players'
        send s r = (join s r) ++ [say' (Str s) "role" $ name r]
        join s r = if r == werewolf then [say'' s] else []
        say'' s  = say' (Str s) "join" "" ++ [Night]
        actions' = [Str "MODE", Day, Str "+m"] : (say "endstart") : (actions g)
        players' = foldr change (players g) wolves
        wolves   = take (div l 3) $ nub $ randomRs (0, l - 1) (gen g)
        l        = size (players g)
        change x = \m -> insert (fst $ elemAt x m) werewolf m

-- }}}

-- Tool functions {{{

-- Talk to IRC thanks to the lanf function
say' :: Action -> String -> String -> [Action]
say' a s arg = [Str "PRIVMSG", a, Str $ ':' : (lang s arg)]

-- Talk to the day channel
say :: String -> [Action]
say = flip (say' Day) ""

-- Talk to the day channel, with an argument string
sayArg :: String -> String -> [Action]
sayArg = say' Day

-- Talk to the night channel
sayNight :: String -> [Action]
sayNight = flip (say' Night) ""

-- Return the timestamp corresponding to when the time will be out
delay :: String -> IO Integer
delay str = do
    TOD clock _ <- getClockTime
    return $ time str + clock

addPlayer :: String -> Game -> Game
addPlayer p g = g { players = insert p villager (players g)
                  , actions = [Str "MODE", Day, Str "+v", Str p] : (actions g)}

-- Make someone vote
vote :: String -> String -> Game -> Game
vote p n g = g {votes = insert p n (votes g)}

-- Give the result of the votes
result :: Game -> String
result g = fst $ elemAt 0 (players g) -- TODO this is not the real function

-- Tell if a game is finished
finished :: Game -> Bool
finished g = bad == p || bad == empty
    where p   = players g
          bad = filter (== werewolf) p

-- }}}

-- General game functions {{{

-- Sleep phase, wait for someone to start the game
wait :: [String] -> String -> Action -> Game -> IO Game
wait ("start":_) _ _ g = delay "wait" >>= \t -> return $ g { timeout = t
                                                           , eval = go
                                                           , actions = say
                                                           "start":(actions g)}
wait _           _ _ g = return g

-- Wait for people to join the game
go :: [String] -> String -> Action -> Game -> IO Game
go ("play":_) p _ = return . addPlayer p
go _          _ _ = return

die :: String -> Game -> Game
die player g = handle (players g ! player) "die" global g
    where global g' = g' { actions = (sayArg "dead" player) : (actions) g'
                         , players = delete player $ players g' }

-- }}}

-- Cards {{{

-- A simple card reprenting the villagers
villager :: Card
villager = Card (lang "villager" "") (flip const) begin'
    where
        begin' g = if finished g then end g else return g { votes = empty
                                                          , actions = act g }
        act g = v g ++ (actions g)
        v g = map (\p -> [Str "MODE", Day, Str "+v", Str p]) $ keys (players g)
        end g = return g { players = empty
                         , timeout = 4294967296
                         , eval    = wait
                         , next    = start
                         , actions = [Str "MODE", Day, Str "-m"] : (say "end")
                         : (actions g) }

-- A card representing the werewolves
werewolf :: Card
werewolf = Card (lang "werewolf" "") (flip const) begin'
    where
        begin' g = delay "werewolf" >>= \t -> return $ g { timeout = t
                                                         , votes = empty
                                                         , eval = eval'
                                                         , actions = actions' g
                                                         , next = next'}
        eval' ("vote":n:_) p Night g = return $ vote p n g
        eval' _            _ _     g = return g
        actions' g = say "sleep" : (v g) ++ (sayNight "wakeup" : (actions g))
        v g = map (\p -> [Str "MODE", Day, Str "-v", Str p]) $ keys (players g)
        next' g = begin villager $ die (result g) $ g {actions = sleep g}
        sleep g = (sayNight "sleep") : (actions g)

-- }}}
