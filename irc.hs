-- This file is inspired from:
-- http://www.haskell.org/haskellwiki/Roll_your_own_IRC_bot

module IRC (start) where

-- Import {{{
import Control.Exception    (bracket, catch, SomeException (SomeException))
import Control.Monad.State  (StateT, evalStateT, gets, modify, liftIO)
import Network              (connectTo, PortID (PortNumber), PortNumber)
import Prelude              hiding (catch)
import System.Exit          (exitWith, ExitCode (ExitSuccess))
import System.IO            (Handle, hSetBuffering, hClose)
import System.IO            (hGetLine, BufferMode (NoBuffering))
import Text.Printf          (printf, hPrintf)
import Werewolves           (Game, newGame, eval, actions, update, Action (..))
-- }}}

-- Define types {{{
type Net = StateT Bot IO
data Bot = Bot {chan :: String, nick :: String, socket :: Handle, game :: Game}
-- }}}

-- Core functions {{{

-- Run the main loop, after connecting, and disconnect after
start :: String -> PortNumber -> String -> String-> IO ()
start server port ch ni = bracket (connect server port ch ni) disconnect loop
    where disconnect = hClose . socket
          loop st = catch (evalStateT run st) (\(SomeException _) -> return ())

-- Connect to the server and return the initial bot state
connect :: String -> PortNumber -> String -> String -> IO Bot
connect server port chan' nick' = do
    handle <- connectTo server $ PortNumber port
    hSetBuffering handle NoBuffering
    newGame >>= return . (Bot chan' nick' handle)

-- Identify to the network, and go to the listen loop
run :: Net ()
run = do
    n <- gets nick
    write "NICK" n
    write "USER" (n ++ " 0 * :werewolves bot")
    c <- gets chan
    write "JOIN" c
    write "JOIN" (c ++ "-night")
    gets socket >>= listen

-- The main loop
listen :: Handle -> Net ()
listen h = forever $ process h >> handleTimeout >>= interpret' >> clean
    where
        interpret' = \g -> modify (\b -> b {game = g}) >> interpret g
        forever a = a >> forever a
        handleTimeout = gets game >>= liftIO . update
        clean = gets game >>= \g -> modify $ \b -> b {game = g {actions = []}}

-- Send a message out to the server we're currently connected to
write :: String -> String -> Net ()
write s t = do
    h <- gets socket
    _ <- liftIO $ hPrintf h "%s %s\r\n" s t
    liftIO $ printf    "> %s %s\n" s t

-- }}}

-- Parse functions {{{

-- Process each line from the server
process :: Handle -> Net()
process h = do
    s <- fmap init $ liftIO (hGetLine h)
    liftIO (putStrLn s)
    if take 6 s == "PING :"
       then write "PONG" $ ':' : (drop 6 s)
       else parse (nick' s) (chan' s) $ words $ clean s
           where
               clean = drop 1 . dropWhile (/= ':') . drop 1
               nick' = takeWhile (/= '!') . drop 1
               chan' = (!! 2) . words

-- Interpret the actions required by the game
interpret :: Game -> Net ()
interpret g = gets chan >>= \c -> sequence_ $ map (say . toString c) list
    where
        list                    = reverse $ actions g
        toString _ []           = []
        toString c ((Str s):xs) = s:(toString c xs)
        toString c (Day:xs)     = c:(toString c xs)
        toString c (Night:xs)   = (c ++ "-night"):(toString c xs)
        say (x:xs)              = write x $ unwords xs
        say _                   = return ()

-- Eval the command received through the server
parse :: String -> String -> [String] -> Net ()
parse _ _ ("!quit":_)= write "QUIT" ":Exiting" >> liftIO (exitWith ExitSuccess)
parse n c (('!':xs):w) = do
    g <- gets game
    c' <- gets chan
    let a = if c == c' then Day else if c == c'++"-night" then Night else Str""
    g' <- liftIO $ eval g (xs:w) n a g
    modify $ \b -> b {game = g'}

parse _ _ _            = return ()

-- }}}
